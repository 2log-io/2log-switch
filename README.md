# 2log-switch
The 2log switch firmware for the Shelly Plug S


# Flashing with esptool.py

esptool.py --chip esp8266 --port /dev/tty.usbserial-UUT1 --baud 230400 --before default_reset --after hard_reset write_flash -z --flash_mode dio --flash_freq 40m --flash_size 4MB 0x0 /src/build/bootloader/bootloader.bin 0x10000 /src/build/2log-switch.bin 0x8000 /src/build/partitions-single-1MB.bin
